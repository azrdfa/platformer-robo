extends Node2D

var in_game = true

func _init():
	global.current_level = 1
	
func _ready():
	global.current_level = 1
	global.enemies = 4
	$CanvasLayer/MarginContainer/VBoxContainer/Label3.hide()
	$Player.scene_to_load = "Level1"
	$Forbidden.scene_to_load = "Level1"

func _process(delta):
	if global.enemies == 0 && in_game:
		in_game = false
		$Player/Sound/BackGround.stop()
		$CanvasLayer/MarginContainer/VBoxContainer/Label3.show()
		$Timer.start()
		

func _on_Timer_timeout():
	get_tree().change_scene("res://Scene/Level/Level2.tscn")
