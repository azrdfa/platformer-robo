extends Node2D

var in_game = true

func init():
	global.current_level = 2
	
func _ready():
	global.current_level = 2
	global.enemies = 4
	$CanvasLayer/MarginContainer/VBoxContainer/Label3.hide()
	$Player.scene_to_load = "Level2"
	$Forbidden.scene_to_load = "Level2"

func _process(delta):
	if global.enemies == 0 && in_game:
		$Player/Sound/BackGround.stop()
		in_game = false
		$CanvasLayer/MarginContainer/VBoxContainer/Label3.show()
		$Timer.start()

func _on_Timer_timeout():
	get_tree().change_scene("res://Scene/GUI/Win/Win.tscn")
