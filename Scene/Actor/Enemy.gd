extends KinematicBody2D


var gravity = 1200
var velocity = Vector2()
var direction = 1
export (int) var speed = 200
export (int) var hp = 1
export (Vector2) var size = Vector2(1,1)

var is_dead = false
var player_is_dead = false

const UP = Vector2(0,-1)
onready var animator = self.get_node("AnimatedSprite")

func _ready():
	scale = size
	
func dead():
	hp -= 1
	$Sound/Shooted.play()
	if hp <= 0:
		is_dead = true
		global.energy += 2
		global.enemies -= 1
		velocity = Vector2(0, 0)
		animator.play("dead")
		$Timer.start()
		$CollisionShape2D.call_deferred("set_disabled", true)
		if scale > Vector2(1, 1):
			get_parent().get_node("ScreenShake").screen_shake(1, 10, 100) 
	
func _physics_process(delta):
	if !is_dead:
		if direction == 1:
			animator.flip_h = false
		else:
			animator.flip_h = true
		animator.play("walk")
		velocity.x = speed * direction
		velocity.y += gravity
		velocity = move_and_slide(velocity, UP)
		
	if is_on_wall():
		direction *= -1
		$RayCast2D.position.x *= -1
	
	if !$RayCast2D.is_colliding():
		direction *= -1
		$RayCast2D.position.x *= -1
	
	if get_slide_count() > 0 && !player_is_dead:
		for i in range(get_slide_count()):
			if "Player" in get_slide_collision(i).collider.name:
				get_slide_collision(i).collider.dead()
				player_is_dead = true

func _on_Timer_timeout():
	queue_free()
