extends KinematicBody2D

export (int) var speed = 200
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400
export (int) var jump_max = 3

const UP = Vector2(0,-1)
const FIREBALL = preload("res://Scene/Component/Fireball2.tscn")

var velocity = Vector2()
var jump_counter = 0
var is_attacking = false
var is_dashing = false
var is_dead = false
var music_playing = false

onready var animator = self.get_node("AnimatedSprite")
onready var scene_to_load

func _ready():
	$Sound/BackGround.play()

# helper method
func dash():
	if !is_dashing:
		$Sound/Dash.play()
		is_dashing = true
		speed = 400
		$DashTimer.start()

func jump_animation():
	if sign($Position2D.position.x) == 1:
		animator.flip_h = false
	else:
		animator.flip_h = true
	animator.play("jump")

func dead():
	is_dead = true
	$Sound/BackGround.stop()
	$Sound/Dead.play()
	velocity = Vector2(0, 0)
	animator.play("dead")
	$CollisionShape2D.call_deferred("set_disabled", true)
	$DeadTimer.start()

func get_input():
	velocity.x = 0
		
	if Input.is_action_pressed('ui_right'):
		if !is_attacking:
			velocity.x += speed
			animator.play("walk")
			animator.flip_h = false
			if sign($Position2D.position.x) == -1:
				$Position2D.position.x *= -1
			
	elif Input.is_action_pressed('ui_left'):
		if !is_attacking:
			velocity.x -= speed
			animator.play("walk")
			animator.flip_h = true
			if sign($Position2D.position.x) == 1:
				$Position2D.position.x *= -1

	else:
		if !is_attacking:
			animator.play("stand")
			if sign($Position2D.position.x) == 1:
				animator.flip_h = false
			else:
				animator.flip_h = true
	
	if Input.is_action_just_pressed('ui_up'):
		if !is_attacking:
			if is_on_floor():
				velocity.y = jump_speed
				jump_counter += 1
				$Sound/Jump.play()
			else:
				if jump_counter < jump_max && jump_counter != 0:
					velocity.y = jump_speed
					jump_counter += 1
					$Sound/Jump.play()
	
	if Input.is_action_just_pressed('ui_focus_next'):
		if !is_attacking:
			dash()
			
	if Input.is_action_just_pressed("ui_select") && !is_attacking:
		is_attacking = true
		animator.play("attack")
		$Sound/Laser.play()
		var fireball = FIREBALL.instance()
		if sign($Position2D.position.x) == -1:
			fireball.set_fireball_direction(-1)
			animator.flip_h = true
		else:
			animator.flip_h = false
		get_parent().add_child(fireball)
		fireball.position = get_node("Position2D").global_position
	
	if get_slide_count() > 0:
		for i in range(get_slide_count()):
			if "Enemy" in get_slide_collision(i).collider.name:
				print(i)
				dead()
			
func _physics_process(delta):
	if is_on_floor():
		jump_counter = 0
	if !is_dead:
		get_input()
		velocity.y += delta * GRAVITY
		velocity = move_and_slide(velocity, UP)
		
func _process(delta):
	if velocity.y != 0 && !is_attacking:
		jump_animation()
	if speed == 400:
		animator.play("dash")

# signal method
func _on_AnimatedSprite_animation_finished():
	is_attacking = false
	
func _on_Timer_timeout():
	is_dashing = false
	speed = 200

func _on_DeadTimer_timeout():
	global.lives -= 1
	if global.lives <= 0:
		get_tree().change_scene(str("res://Scene/GUI/GameOver/GameOver.tscn"))
	else:
		get_tree().change_scene(str("res://Scene/Level/" + scene_to_load + ".tscn"))
