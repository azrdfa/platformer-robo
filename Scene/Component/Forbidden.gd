extends Area2D

export (String) var scene_to_load

func _on_Area2D_body_exited(body):
	if body.get_name() == "Player":
		global.lives -= 1
		if (global.lives <= 0):
			get_tree().change_scene(str("res://Scene/GUI/GameOver/GameOver.tscn"))
		else:
			get_tree().change_scene(str("res://Scene/Level/" + scene_to_load + ".tscn"))
			
